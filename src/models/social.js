import bookshelf from '../../config/database/bookshelf';
import User from './user';

const Social = bookshelf.Model.extend({
    tableName    : 'socials',

    users: function () {
        return this.belongsToMany(User, 'user_socials');
    },

});

export default bookshelf.model('Social', Social);