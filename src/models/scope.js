import bookshelf from '../../config/database/bookshelf';
import User from './user';

const Scope = bookshelf.Model.extend({
    tableName    : 'scopes',

    users: function () {
        return this.belongsToMany(User, 'user_roles');
    },

});

export default bookshelf.model('Scope', Scope);