import bookshelf from '../../config/database/bookshelf';
import { UserScope }  from './user-scope';
import { Base } from './base';

const EventModel = Base.extend( {
    tableName:     'events',
    hasTimestamps: true,

    creator: function () {
        return this.belongsTo( UserScope );
    },
    //
    // images: function () {
    //     return this.hasMany('Image');
    // }
}, {
    sortable: [ 'id' ],
} );

export const Event = bookshelf.model( 'Event', EventModel );

export const Events = bookshelf.Collection.extend( {
    model: Event
} );