import bookshelf from '../../config/database/bookshelf';

const Image = bookshelf.Model.extend({
    tableName    : 'images',
    hasTimestamps: true,

    event: function () {
        return this.belongsTo('Event');
    },

});

export default bookshelf.model('Image', Image);