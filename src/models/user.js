import bookshelf from '../../config/database/bookshelf';
import Promise from 'bluebird';
import bcrypt from 'bcrypt';
import _ from 'lodash';
import Event from './event';
import Scope from './scope';
import Social from './social';
import { UserScope } from './user-scope';
import { Base } from './base';

const UserModel = Base.extend({
    tableName    : 'users',
    hasTimestamps: true,

    /**
     * User scopes
     *
     * @returns {*|Collection}
     */
    scopes: function () {
        return this.belongsToMany(Scope, 'user_scopes');
    },

    userScopes: function () {
        return this.hasMany(UserScope);
    },

    /**
     * User socials
     *
     * @returns {*|Collection}
     */
    socials: function () {
        return this.belongsToMany(Social, 'user_socials');
    },

    /**
     *
     */
    initialize: function () {
        this.on('creating', this._hashPassword, this);
        this.on('saving', this._assertEmailUnique, this)
    },

    /**
     *
     * @param model
     * @param attrs
     * @param options
     * @private
     */
    _hashPassword     : (model, attrs, options) => {
        return new Promise((resolve, reject) => {
            if (model.attributes.password) {
                bcrypt.hash(model.attributes.password, 10, (err, hash) => {
                    if (err) {
                        reject(err);
                    }
                    model.set('password', hash);
                    resolve(hash);
                });
            } else {
                resolve();
            }
        });
    },
    /**
     *
     * @param model
     * @param attributes
     * @param options
     * @private
     */
    _assertEmailUnique: function (model, attributes, options) {
        if (this.hasChanged('email')) {
            return new Promise((resolve, reject) => {
                return UserModel
                    .query({ where: { email: this.get('email') } })
                    .fetch(_.pick(options || {}, 'transacting'))
                    .then(function (existing) {
                        if (existing) {
                            reject({ status: 400, email: 'User with such email already exists' });
                        } else {
                            resolve();
                        }
                    });
            });
        }
    },

    /**
     * Validate user password
     *
     * @param password
     */
    validatePassword: function (password) {

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, this.get('password'), (err, isValid) => {
                if (err) {
                    reject(err)
                }
                if (isValid) {
                    resolve(isValid);
                } else {
                    reject({ status: 400, message: 'Wrong password' });
                }
            });
        });
    }
}, {
    sortable: [ 'id' ],
});

export default bookshelf.model('User', Base.extend({
    tableName    : 'users',
    hasTimestamps: true,

    /**
     * User scopes
     *
     * @returns {*|Collection}
     */
    scopes: function () {
        return this.belongsToMany(Scope, 'user_scopes');
    },

    userScopes: function () {
        return this.hasMany(UserScope);
    },

    /**
     * User socials
     *
     * @returns {*|Collection}
     */
    socials: function () {
        return this.belongsToMany(Social, 'user_socials');
    },

    /**
     *
     */
    initialize: function () {
        this.on('creating', this._hashPassword, this);
        this.on('saving', this._assertEmailUnique, this)
    },

    /**
     *
     * @param model
     * @param attrs
     * @param options
     * @private
     */
    _hashPassword     : (model, attrs, options) => {
        return new Promise((resolve, reject) => {
            if (model.attributes.password) {
                bcrypt.hash(model.attributes.password, 10, (err, hash) => {
                    if (err) {
                        reject(err);
                    }
                    model.set('password', hash);
                    resolve(hash);
                });
            } else {
                resolve();
            }
        });
    },
    /**
     *
     * @param model
     * @param attributes
     * @param options
     * @private
     */
    _assertEmailUnique: function (model, attributes, options) {
        if (this.hasChanged('email')) {
            return new Promise((resolve, reject) => {
                return UserModel
                    .query({ where: { email: this.get('email') } })
                    .fetch(_.pick(options || {}, 'transacting'))
                    .then(function (existing) {
                        if (existing) {
                            reject({ status: 400, email: 'User with such email already exists' });
                        } else {
                            resolve();
                        }
                    });
            });
        }
    },

    /**
     * Validate user password
     *
     * @param password
     */
    validatePassword: function (password) {

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, this.get('password'), (err, isValid) => {
                if (err) {
                    reject(err)
                }
                if (isValid) {
                    resolve(isValid);
                } else {
                    reject({ status: 400, message: 'Wrong password' });
                }
            });
        });
    }
}, {
    sortable: [ 'id' ],
}));

export const Users = bookshelf.Collection.extend({
    model: UserModel
});