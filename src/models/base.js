import bookshelf from '../../config/database/bookshelf';
import { mapSort } from '../http/query-params/sort';
import Promise from 'bluebird';
import { map } from 'ramda';
/**
 * Base model
 *
 * @type {*}
 */
export const Base = bookshelf.model( 'Base', bookshelf.Model.extend( {}, {

    /**
     * Find all records
     *
     * @param query
     */
    findAll( query ) {
        try {
            return this
                .query( qb => {
                    /**
                     * Apply sort query
                     */
                    map( sortObj => qb.orderBy( sortObj.column, sortObj.order ),
                        mapSort( query.sort, this.sortable )
                    );
                } )
                .fetchPage( {
                    page: query.page,
                    pageSize: query.pageSize
                } )

                .then( result => {
                    return {
                        data: result.toJSON( { omitPivot: true } ),
                        meta: result.pagination
                    }
                } )
        } catch ( e ) {
            return new Promise( () => {
                throw e;
            } )
        }

    }
} ) );
