import bookshelf from '../../config/database/bookshelf';
import User from './user';
import {Event} from './event';

const UserScopeModel = bookshelf.Model.extend({
    tableName    : 'user_scopes',

    /**
     *
     * @returns {*|Model}
     */
    user: function () {
        return this.belongsTo(User);
    },
    /**
     *
     */
    events: function () {
        return this.hasMany(Event);
    }

});

export const UserScope = bookshelf.model('UserScope', UserScopeModel);