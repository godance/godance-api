import Promise from 'bluebird';
import User from '../models/user';
import Scope from '../models/scope';
import { scopes } from '../../config/constants';
import R from 'ramda';

/**
 * Add user scope
 *
 * @param user
 * @param scope
 * @returns {Promise.<TResult>}
 */
export function addScope(user, scope) {

    return _findScope(scope)
        .then(scope => {

            return user
                .scopes()
                .fetch()
                .then(scopes => {

                    const scopesJSON = scopes.toJSON();

                    if (scopesJSON.length > 0 && _scopeIdExists(scope.get('id'), scopesJSON)) {
                        return user;
                    }

                    return user
                        .scopes()
                        .attach({ scope_id: scope.get('id'), is_active: true })
                        .then(() => user);

                })

        })

}

/**
 * Remove user scope
 *
 * @param user
 * @param scope
 * @returns {Promise.<TResult>}
 */
export function removeRole(user, scope) {
    return _findScope(scope)
        .then(scope => {
            return user
                .scopes()
                .detach({ scope_id: scope.get('id') })
                .then(() => user);
        })

}
/**
 *
 * @param user
 * @returns {Promise.<TResult>}
 */
export function addAdminScope(user) {

    if (R.contains(user.get('email'), process.env.ADMINS.split('|'))) {
        return addScope(user, scopes.ADMIN);
    }
    return user;
}
/**
 *
 * @param user
 * @returns {Promise.<TResult>}
 */
export function addUserScope(user) {
    return addScope(user, scopes.USER);
}

/**
 * Find scope
 *
 * @param scope
 * @returns {*|Promise.<Model|null>}
 * @private
 */
function _findScope(scope) {
    return Scope
        .forge({ scope })
        .fetch()
        .then(scope => {
            if (!scope) {
                throw new Error('No such scope')
            }
            return scope
        })
}
/**
 *
 *
 * @param scopeId
 * @param scopes
 * @private
 */
function _scopeIdExists(scopeId, scopes) {
    return R.contains(scopeId, R.pluck('id')(scopes))
}