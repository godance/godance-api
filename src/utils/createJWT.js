import jwt from 'jsonwebtoken';
import Promise from 'bluebird';
import R from 'ramda';

/**
 * Create JWT token
 *
 * @param user
 */
export function createJWT(user) {

    return user
        .scopes()
        .fetch()
        .then(scopes => {

            return new Promise((resolve, reject) => {

                /**
                 * Create JWT token
                 */
                jwt.sign({
                        id   : user.get('id'),
                        fn   : user.get('first_name'),
                        ln   : user.get('last_name'),
                        scope: R.map(scope => scope.toLowerCase(), R.pluck('scope')(scopes.toJSON()))
                    },
                    process.env.JWT_KEY,
                    { algorithm: process.env.JWT_ALGORITHM },
                    (err, token) => {

                        if (err) {
                            reject(err);
                        }
                        resolve(token);
                    });
            });

        });

}