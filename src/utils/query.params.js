import QueryParser from 'jsonapi-query-parser';
const queryParser = new QueryParser();

export function applyQueryParams(request) {
    return query => {
        /**
         *
         */
        if (request.query.filter) {

            query.where(request.query.filter);
        }

        /**
         *
         */
        if (request.query.like) {
            Object.keys(request.query.like).forEach(likeItem => {

                query.where(likeItem, 'LIKE', '%' + request.query.like[ likeItem ] + '%');
            })
        }
    };
}