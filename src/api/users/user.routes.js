import * as UserController from '../../http/controllers/user.controller';
import * as UserValidator from '../../http/validators/user.validator';

export default [

    /**
     *
     */
    {
        method : 'GET',
        path   : '/users',
        handler: UserController.index,
        config : {
            validate: UserValidator.index,
            auth    : 'jwt'
        }
    },

    /**
     *
     */
    {
        method : 'POST',
        path   : '/users',
        handler: UserController.store,
        config : {
            validate: UserValidator.store
        }
    },

    /**
     *
     */
    {
        method : 'GET',
        path   : '/users/{userId}',
        handler: UserController.show,
        config : {
            validate: UserValidator.show
        }
    },

    /**
     *
     */
    {
        method : 'PUT',
        path   : '/users/{userId}',
        handler: UserController.update,
        config : {
            validate: UserValidator.update
        }
    },

    /**
     *
     */
    {
        method : 'DELETE',
        path   : '/users/{userId}',
        handler: UserController.remove,
        config : {
            validate: UserValidator.remove
        }
    },

    /**
     *
     */
    {
        method : 'POST',
        path   : '/users/login',
        handler: UserController.login,
        config : {
            validate: UserValidator.login
        }
    },
    /**
     *
     */
    {
        method : 'GET',
        path   : '/users/myself',
        handler: UserController.getMyself,
        config : {
            auth    : 'jwt'
        }
    }
]
