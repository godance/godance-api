import * as AuthController from '../../http/controllers/auth.controller';
import * as AuthValidator from '../../http/validators/auth.validator';

export default [

    /**
     * Auth via facebook
     */
    {
        method : 'POST',
        path   : '/auth/facebook',
        handler: AuthController.facebook,
        config : {
            validate: AuthValidator.facebook,
        }
    }
];