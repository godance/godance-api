import * as EventController from '../../http/controllers/event.controller';

export default [
    {
        method : 'GET',
        path   : '/events',
        handler: EventController.getEvents,
        config : {
            validate: {}
        }
    },
]