import { knexInstance as knex } from '../../config/database/bookshelf';

export const relatedModels = {
    /**
     * Roles
     *
     * @param qb
     */
    scopes : function (qb) {
        qb.column('user_scopes.scope_id as id');
        qb.column('user_scopes.is_active');
        qb.column(knex.raw('lower(scope) as scope'));
    },
    /**
     * Socials
     *
     * @param qb
     */
    socials: function (qb) {
        qb.column('user_socials.user_id as user_id');
        qb.column(knex.raw('lower(name) as name'));
        qb.column('user_socials.link');

    }

};