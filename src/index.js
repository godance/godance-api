require('dotenv').config();

import Hapi from 'hapi';
import { routes } from './http/routes';
import { isValidUser } from './jwtValidate';

const server = new Hapi.Server();

/**
 * Setup server
 */
server.connection({
    port  : process.env.APP_PORT,
    routes: {
        cors: true
    }
});

/**
 * Register plugins
 */
server.register([

        /**
         * Hapi qs. Query params
         */
        {
            register: require('hapi-qs'),
        },

        /**
         * JWT auth
         */
        {
            register: require('hapi-auth-jwt2'),
        }
    ],

    err => {

        /**
         * Set auth strategy
         */
        server.auth.strategy('jwt', 'jwt',
            {
                key          : process.env.JWT_KEY,
                validateFunc : isValidUser,
                verifyOptions: { algorithms: [ process.env.JWT_ALGORITHM ] }
            });

        /**
         * Import routes
         */
        server.route(routes);

        /**
         * Start server
         */
        server.start(() => {

            console.log('Server running at: ', server.info.uri);
        });

    });




