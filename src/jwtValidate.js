import User from './models/user';
import R from 'ramda';

/**
 * Check is user valid
 *
 * @param decoded
 * @param request
 * @param callback
 */
export function isValidUser(decoded, request, callback) {

    User
        .query({ where: { id: decoded.id } })
        .fetch({ withRelated: [ 'scopes' ] })
        .then(user => {

            if (!user) {
                return callback(null, false);
            }

            request.auth.user = user;

            return callback(null, true, { scope: _scopes(user) });

        })
        .catch(err => {
            return callback(err, false);
        })

}
/**
 * Get user scopes
 *
 * @param user
 * @private
 */
export function _scopes(user) {
    return R.map(scopeObject => {
        return scopeObject.scope.toLowerCase()
    }, user.toJSON().scopes)
}