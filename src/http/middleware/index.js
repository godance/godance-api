import User from '../../models/user'
import Social from '../../models/social'

/**
 * Check if record exists
 *
 * @param type
 * @returns {Function}
 */
export function existing(type) {

    const types = {
        'user'  : User,
        'social': Social
    };

    const params = {
        'user'  : 'userId',
        'social': 'socialId'
    };

    const model = types[ type ];
    const param = params[ type ];

    return function (request, reply) {

        const id = request.params[ param ];

        model
            .forge({ id })
            .fetch()
            .then(result => {
                if (result) {
                    return reply(result);
                }
                reply();
            });
    }
}