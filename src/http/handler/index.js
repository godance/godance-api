import BadRequestError from '../errors/bad-request.error';
import Boom from 'boom';
/**
 * Error handling
 *
 * @param error
 */
export function handle ( error ) {
    if ( error instanceof BadRequestError ) {

        return Boom.badRequest( error.message );
    }
}
