import ExtendableError from 'es6-error';
/**
 *
 */
class BadRequestError extends ExtendableError {
    constructor ( message ) {
        super( message );
    }
}

export default BadRequestError;