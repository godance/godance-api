import * as UserController from './controllers/user.controller';
import * as UserValidator from './validators/user.validator';
import * as AuthController from "./controllers/auth.controller";
import * as AuthValidator from "./validators/auth.validator";
import * as SocialController from './controllers/social.controller';
import * as EventController from './controllers/event.controller';

import { isUserExists } from './middleware';
import { existing } from "./middleware/index";

export const routes = [

    /**
     *
     * USER ROUTES
     *
     */

    /**
     * Get users
     */
    {
        method: 'GET', path: '/users', handler: UserController.index,
        config: {
            validate: UserValidator.index,
            // auth    : 'jwt'
        }
    },

    /**
     * Create user
     */
    {
        method: 'POST', path: '/users', handler: UserController.store,
        config: {
            validate: UserValidator.store
        }
    },

    /**
     * Get user
     */
    {
        method: 'GET', path: '/users/{userId}', handler: UserController.show,
        config: {
            validate: UserValidator.show
        }
    },

    /**
     * Update user
     */
    {
        method: 'PUT', path: '/users/{userId}', handler: UserController.update,
        config: {
            validate: UserValidator.update
        }
    },

    /**
     * Delete user
     */
    {
        method: 'DELETE', path: '/users/{userId}', handler: UserController.remove,
        config: {
            validate: UserValidator.remove
        }
    },

    /**
     *
     */
    {
        method : 'POST',
        path   : '/users/login',
        handler: UserController.login,
        config : {
            validate: UserValidator.login
        }
    },
    /**
     *
     */
    {
        method : 'GET',
        path   : '/users/myself',
        handler: UserController.getMyself,
        config : {
            auth: 'jwt'
        }
    },

    /**
     *
     */
    {
        method : 'PUT',
        path   : '/users/myself',
        handler: UserController.updateMyself,
        config : {
            auth: 'jwt'
        }
    },

    /**
     *
     * AUTH ROUTES
     *
     */

    /**
     * Auth via facebook
     */
    {
        method : 'POST',
        path   : '/auth/facebook',
        handler: AuthController.facebook,
        config : {
            validate: AuthValidator.facebook,
        }
    },

    /**
     *
     * Socials routes
     *
     */

    /**
     * Create user social
     */
    {
        method: 'POST', path: '/users/{userId}/socials', handler: SocialController.store,
        config: {
            // validate: UserValidator.store
        }
    },
    /**
     * Update user social
     */
    {
        method: 'PUT', path: '/users/{userId}/socials', handler: SocialController.update,
        config: {
            pre: [
                {
                    method: existing('user'), assign: 'user'
                }
            ],
            // validate: UserValidator.store
        }
    },
    /**
     * Update user social
     */
    {
        method: 'DELETE', path: '/users/{userId}/socials/{socialId}', handler: SocialController.remove,
        config: {
            pre: [
                {
                    method: existing('user'), assign: 'user'
                },
                {
                    method: existing('social'), assign: 'social'
                }
            ]
            // validate: UserValidator.store
        }
    },

    /**
     *
     * EVENT ROUTES
     *
     */

    /**
     * Get events
     */
    {
        method: 'GET', path: '/events', handler: EventController.index,
        // config: {
        //     auth: {
        //         strategy: 'jwt',
        //         scope   : [ 'user' ]            // user or admin
        //     },
        //
        // }
    },
    /**
     * Store event
     */
    {
        method: 'POST', path: '/events', handler: EventController.store,
        config: {
            auth: {
                strategy: 'jwt',
                scope   : [ 'user' ]            // user or admin
            },

        }
    },

];