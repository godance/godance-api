import Joi from 'joi';

/**
 * Facebook auth validators
 *
 * @type {{params: {name: (*)}}}
 */
export const facebook = {
    payload: {
        accessToken: Joi.string().required(),
    }
};