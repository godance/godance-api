import Joi from 'joi';

/**
 *
 * @type {{params: {name: *, test: *}}}
 */
export const index = {
    params: {
        name: Joi.string().min(3).max(10),
    }
};

/**
 *
 * @type {{payload: {email: *, password: *, firstName: *}}}
 */
export const store = {
    payload:{
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        firstName: Joi.string().min(3).max(10),
        lastName: Joi.string().min(3).max(10),
        middleName: Joi.string().min(3).max(10),
        gender: Joi.boolean()
    }
};

/**
 *
 * @type {{params: {name: *}}}
 */
export const show = {
    params: {
        name: Joi.string().min(3).max(10),
    }
};

/**
 *
 * @type {{params: {name: *}}}
 */
export const update = {
    params: {
        userId: Joi.number(),
    }
};

/**
 *
 * @type {{params: {name: *}}}
 */
export const remove = {
    params: {
        name: Joi.string().min(3).max(10),
    }
};

/**
 *
 * @type {{payload: {email: *, password: *}}}
 */
export const login = {
    payload:{
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
    }
};