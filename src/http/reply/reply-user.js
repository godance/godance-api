import User from '../../models/user';
import { relatedModels } from '../../related-models'
/**
 * Reply user
 *
 * @param user
 * @param reply
 */
export function replyUser(user, reply) {

    return User
        .forge({ id: user.get('id') })
        .fetch({ withRelated: [ relatedModels ] })
        .then(user => reply(user.toJSON({ omitPivot: true })));

    //
    //
    // let userObject = {
    //     id        : user.get('id'),
    //     firstName : user.get('first_name'),
    //     lastName  : user.get('last_name'),
    //     middleName: user.get('middle_name'),
    //     email     : user.get('email'),
    //     gender    : user.get('gender'),
    //     birthday  : user.get('birthday'),
    // };
    //
    // return user
    //     .s()
    //     .fetch()
    //     .then(s => {
    //         userObject.s = s
    //             .toJSON()
    //             .map(role => ({
    //                 id: role.id,
    //                 role: role.role.toLowerCase()
    //             }));
    //         reply(userObject);
    //     })

}