import R from 'ramda';
const { difference, filter, map, pipe, length } = R;
import BadRequestError from '../errors/bad-request.error';

/**
 * Main function
 *
 * @param sort
 * @param sortable
 */
export function mapSort ( sort, sortable = [] ) {
    const sortArray = sortToArray( sort );
    const diff = getDiff( sortArray, sortable );
    if ( length( diff ) > 0 ) {
        throw new BadRequestError( `Sort fields ${diff} are not accepted` );
    }
    return map( _mapSortItems, sortArray );
}

export const sortToArray = sort => sort.split( ',' );

export function getDiff ( sort, sortable ) {
    const deDashed = mapSortToDedash( sort );
    return pipe( difference )( deDashed, sortable );
}

export const deDash = item => item.replace( /^-/g, '' );
export const mapSortToDedash = map( deDash );

/**
 *
 *
 * @param item
 * @returns {*}
 * @private
 */
export function _mapSortItems ( item ) {
    if ( item[ 0 ] === '-' ) {
        return {
            order:  'DESC',
            column: item.substring( 1 )
        }
    }
    return {
        order:  'ASC',
        column: item
    }
}

/**
 *
 * @param item
 * @returns {string|*|void|XML}
 * @private
 */
export function _deDash ( item ) {
    return item.replace( /^-/g, '' )
}