import { relatedModels } from '../../related-models'
import { mapSort as _sort } from './sort';

/**
 *
 * @param filter
 * @param qb
 * @returns {*}
 */
export function filter(filter, qb) {
    qb.where(filter);
    return qb;
}
/**
 *
 * @param like
 * @param qb
 * @returns {*}
 */
export function like(like, qb) {
    if (like instanceof Array) {
        Object.keys(like).forEach(likeItem => {
            qb.where(likeItem, 'LIKE', '%' + like[ likeItem ] + '%');
        })
    }
    return qb
}

/**
 *
 *
 * @param fields
 * @param qb
 * @returns {{}}
 */
export function fields(fields) {

    if (fields) {
        fields     = fields.split(',');
        let result = {};

        fields.forEach(field => {

            if (relatedModels[ field ]) {
                result = {
                    ...result,
                    [field]: relatedModels[ field ]
                };
            }
        });

        return result;
    }
}

/**
 *
 *
 * @param request
 * @param opts
 * @returns {function(*=)}
 */
export function queryParams(request, opts) {

    const { sort } = request.query;
    return qb => {
        if (sort) {
            _sort(qb, sort.split(','), opts.sortable);
        }
    }
}