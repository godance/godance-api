import User, { Users } from '../../models/user';
import Role from '../../models/scope';
import Boom from 'boom';
import { applyQueryParams } from '../../utils/query.params';
import jwt from 'jsonwebtoken';
import Promise from 'bluebird';
import { knexInstance as knex } from '../../../config/database/bookshelf';

import Event from '../../models/event';
import Image from '../../models/image.model';
import { createJWT } from '../../utils/createJWT';
import { replyUser } from "../reply/reply-user";

import { addScope } from '../../acl';
import { scopes } from '../../../config/constants';
import { relatedModels } from '../../related-models'
import { fields } from '../query-params';
import { addUserScope } from "../../acl/index";
import { handle } from '../handler'

/**
 * Get all users
 *
 * @param request
 * @param reply
 */
export function index ( request, reply ) {

    User.findAll( request.query )
        .then( users => reply( users ) )
        .catch( err => reply( handle( err ) ) );

    // Users
    //     .forge()
    //     .query(applyQueryParams(request))
    //     .fetch({
    //         withRelated: [ fields(request.query.fields) ]
    //     })
    //     .then(users => {
    //
    //         reply(users.toJSON({ omitPivot: true }));
    //     })
    //     .catch(err => {
    //
    //         reply(Boom.badRequest('Invalid query'));
    //     });

}

/**
 * Create user
 *
 * @param request
 * @param reply
 */
export function store ( request, reply ) {

    const { email, password, firstName, lastName } = request.payload;

    User
        .forge( {
                email,
                password,
                first_name: firstName,
                last_name:  lastName
            },
            { hasTimestamps: true } )
        .save()
        .then( user => addUserScope( user ) )
        .then( user => replyUser( user, reply ) )
        .catch( ( err ) => {
            const status = (err.status) ? err.status : 500;
            reply( Boom.create( status, err.email ) );
        } );

}

/**
 * Get user
 *
 * @param request
 * @param reply
 */
export function show ( request, reply ) {

}

/**
 * Update user
 *
 * @param request
 * @param reply
 */
export function update ( request, reply ) {

    const { userId }              = request.params;
    const {
              firstName,
              lastName,
              middleName,
              gender,
              birthday,
              socials,
          }                       = request.payload;

    User
        .forge( { id: userId } )
        .fetch()
        .then( user => {

            /**
             * If not exist
             */
            if ( !user ) {

                return reply( Boom.notFound( 'Record not exists' ) )
            }

            /**
             * Save user
             */
            return user
                .save( {
                        first_name:  firstName,
                        last_name:   lastName,
                        middle_name: middleName,
                        gender,
                        birthday
                    },
                    { method: 'update' } );

        } )
        .then( user => {

            replyUser( user, reply );
        } )

}

/**
 * Remove user
 *
 * @param request
 * @param reply
 */
export function remove ( request, reply ) {

}

/**
 * Login user
 *
 * @param request
 * @param reply
 */
export function login ( request, reply ) {

    /**
     * Extract payload values
     */
    const { email, password } = request.payload;

    /**
     * Find user
     */
    User
        .query( { where: { email } } )
        .fetch()
        .then( user => {

            if ( !user ) {
                return reply( Boom.badRequest( 'invalid query' ) )
            }
            /**
             * Validate password
             */
            return user.validatePassword( password )
                .then( () => {
                    return createJWT( user )
                } )
        } )
        .then( token => {
            reply( { status: 'success', token } );
        } )
        .catch( err => {

            const status = (err.status) ? err.status : 500;
            reply( Boom.create( status, err.message ) );
        } )
}

/**
 * Get myself info
 *
 * @param request
 * @param reply
 */
export function getMyself ( request, reply ) {
    replyUser( request.auth.user, reply );
}

/**
 * Update myself
 *
 * @param request
 * @param reply
 */
export function updateMyself ( request, reply ) {
    request.params.userId = request.auth.user.get( 'id' );
    update( request, reply );
}

function updateUsers () {

}