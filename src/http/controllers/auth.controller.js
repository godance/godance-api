import graph from 'fbgraph';
import User, { Users } from '../../models/user';
import { createJWT } from '../../utils/createJWT';
import Boom from 'boom';
import { addScope, addAdminScope, addUserScope } from '../../acl';
import { scopes } from '../../../config/constants';

/**
 * Auth with facebook
 *
 * @param request
 * @param reply
 */
export function facebook(request, reply) {

    graph.setAccessToken(request.payload.accessToken);
    graph.get("me?locale=en_US&fields=id,name,email,gender,first_name,last_name", (err, response) => {

        /**
         * Extract user data
         */
        const { email, first_name, last_name } = response;

        /**
         * Search user
         */
        Users
            .query({ where: { email } })
            .fetchOne()
            .then(existingUser => {
                if (existingUser) {
                    return existingUser;
                } else {
                    return User
                        .forge({
                            email,
                            first_name,
                            last_name
                        })
                        .save()
                        .then(user => addUserScope(user))
                }
            })
            .then(user => addAdminScope(user))
            .then(createJWT)
            .then(token => {
                reply({ status: 'success', token: token });
            })
            /**
             * Catch errors
             */
            .catch(err => {
                const status = (err.status) ? err.status : 500;
                return reply(Boom.create(status, err));
            });
    });

}