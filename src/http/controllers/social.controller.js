import Event from '../../models/social';
import User, { Users } from '../../models/user';
import Social from '../../models/social';
import Boom from 'boom';
import { relatedModels } from '../../related-models'
import { knexInstance as knex } from '../../../config/database/bookshelf';

export function index(request, reply) {

}

export function show(request, reply) {

}

/**
 *
 *
 * @param request
 * @param reply
 */
export function store(request, reply) {

    const { userId }     = request.params;
    const { name, link } = request.payload;
    /**
     * Take user
     */
    User
        .forge({ id: userId })
        .fetch()
        .then(user => {

            if (!user) {
                return reply(Boom.notFound('User not exists'))
            }
            /**
             * Take social type
             */
            Social
                .forge({ name: name.toUpperCase() })
                .fetch()
                .then(social => {

                    if (!social) {
                        return reply(Boom.notFound('Social not exists'))
                    }
                    /**
                     * Take if such social exists
                     */
                    return user
                        .socials()
                        .query('where', 'social_id', social.get('id'))
                        .fetch()
                        .then(existing => {

                            if (existing.length > 0) {
                                return reply(Boom.badRequest('Such social already exists'))
                            }

                            return user
                                .socials()
                                .attach({
                                    social_id: social.get('id'),
                                    link
                                })
                                .then(socials => {

                                    return socials.query(relatedModels.socials)
                                        .fetch()
                                        .then(result => {
                                            reply(result.toJSON({ omitPivot: true }))
                                        })
                                })
                        })
                })
        })
}

/**
 * Update social
 *
 * @param request
 * @param reply
 */
export function update(request, reply) {

    if (!request.pre.user) {
        return reply(Boom.notFound('User not exists'));
    }

    const { name, link } = request.payload;

    User
        .forge({ id: userId })  //TODO Refactor store and update
        .fetch()
        .then(user => {

            if (!user) {

            }
            /**
             * Take social type
             */
            Social
                .forge({ name: name.toUpperCase() })
                .fetch()
                .then(social => {

                    if (!social) {
                        return reply(Boom.badRequest('Social type not exists'))
                    }
                    /**
                     * Take if such social exists
                     */
                    return user
                        .socials()
                        .query('where', 'social_id', social.get('id'))
                        .fetch()
                        .then(existing => {

                            if (existing.length === 0) {
                                return reply(Boom.badRequest('No such social'))
                            }

                            return user
                                .socials()
                                .updatePivot({ link }, { query: { where: { social_id: social.get('id') } } })
                                .then(socials => {
                                    return socials.query(relatedModels.socials)
                                        .fetch()
                                        .then(result => {
                                            reply(result.toJSON({ omitPivot: true }))
                                        })
                                })
                        })
                })
        })
}

/**
 * Remove social
 *
 * @param request
 * @param reply
 */
export function remove(request, reply) {

    if (!request.pre.user) {
        return reply(Boom.notFound('User not exists'));
    }
    if (!request.pre.social) {
        return reply(Boom.notFound('Social not exists'));
    }

    const { user, social } = request.pre;

    return user
        .socials()
        .detach({ social_id: social.get('id') })
        .then(() => {
            reply([])
        })

}
