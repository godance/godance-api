import { Event, Events } from '../../models/event';
import { fields } from '../query-params';
import { applyQueryParams } from '../../utils/query.params';
import Boom from 'boom';
import { sort, queryParams } from '../query-params';
import { scopes } from '../../../config/constants';
import Scope from '../../models/scope';

/**
 * Data options
 *
 * @type {{sortable: [*]}}
 */
const opts = {
    sortable: [ 'id' ]
};

/**
 * Get events
 *
 * @param request
 * @param reply
 */
export function index ( request, reply ) {

    Event
        .findAll( request.query )
        // .query(queryParams(request, opts))
        // .fetchPage({
        //     pageSize   : request.query.page_size,
        //     page       : request.query.page,
        //     withRelated: [
        //         {
        //             'creator': qb => {
        //                 qb.column('id');
        //                 qb.column('user_id');
        //                 qb.column('is_active');
        //             }
        //         },
        //         {
        //             'creator.user': qb => {
        //                 qb.column('id');
        //             }
        //         }
        //     ]
        //
        // })
        .then( events => {
            reply( {
                data: events
            } );
        } )
        .catch( err => {
            reply( Boom.badRequest( err ) );
        } );

}

export function show ( request, reply ) {

}

/**
 * Create event
 *
 * @param request
 * @param reply
 */
export function store ( request, reply ) {

    const { user }        = request.auth;
    const { title, info } = request.payload;

    Scope
        .forge( { scope: scopes.USER } )
        .fetch()
        .then( scope => {

            user
                .userScopes()
                .query( 'where', 'scope_id', scope.get( 'id' ) )
                .fetchOne()
                .then( userScope => {

                    userScope
                        .events()
                        .create( { title, info } )
                        .then( event => reply( event ) );

                } )

        } );
}

export function update ( request, reply ) {

}
export function remove ( request, reply ) {

}

