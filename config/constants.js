/**
 * User roles
 *
 * @type {{ADMIN: string, USER: string, DANCER: string, TEACHER: string, SCHOOL: string, PROMOTER: string}}
 */
exports.scopes = {
    ADMIN   : 'ADMIN',
    USER    : 'USER',
    DANCER  : 'DANCER',
    TEACHER : 'TEACHER',
    SCHOOL  : 'SCHOOL',
    PROMOTER: 'PROMOTER',
};
/**
 * User socials
 *
 * @type {{FACEBOOK: string, VK: string, TWITTER: string, OK: string, INSTAGRAM: string}}
 */
exports.socials = {
    FACEBOOK : 'FACEBOOK',
    VK       : 'VK',
    TWITTER  : 'TWITTER',
    OK       : 'OK',
    INSTAGRAM: 'INSTAGRAM',
};
/**
 * User notifications
 *
 * @type {{SMS: number, EMAIL: number, CALL: number}}
 */
exports.notifications = {
    SMS  : 'SMS',
    EMAIL: 'EMAIL',
    CALL : 'CALL',
};