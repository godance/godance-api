import knex from 'knex';
import knexConfig from './knexfile';

export const knexInstance = knex(knexConfig[ process.env.NODE_ENV ]);

const bookshelf = require('bookshelf')(knexInstance);
/**
 * Bookshelf plugins
 */
bookshelf.plugin('registry');
bookshelf.plugin('pagination');

/**
 * Export
 */
export default bookshelf;

