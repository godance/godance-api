exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('users', function (table) {
            table.increments();
            table.string('email').notNullable().unique().index();
            table.string('password').default(null);
            table.string('first_name').default(null);
            table.string('last_name').default(null);
            table.string('middle_name').default(null);
            table.date('birthday').default(null);
            /**
             * Gender
             */
            table.boolean('gender').default(true);
            /**
             * Notification
             */
            table.boolean('notification_sms').default(false);
            table.boolean('notification_email').default(false);
            table.boolean('notification_calls').default(false);
            /**
             *
             */
            table.timestamps();
        })
        /**
         * Social types
         */
        .createTable('socials', function (table) {
            table.increments();
            table.string('name').unique();
        })
        /**
         * User socials
         */
        .createTable('user_socials', function (table) {
            table.integer('user_id')
                .unsigned()
                .references('id')
                .inTable('users');
            table.integer('social_id')
                .unsigned()
                .references('id')
                .inTable('socials');
            table.string('link');
        })
        /**
         * Roles
         */
        .createTable('scopes', function (table) {
            table.increments();
            table.string('scope').unique();
        })
        /**
         * User scopes
         */
        .createTable('user_scopes', function (table) {
            table.increments();
            table.boolean('is_active').default(false);
            table.integer('user_id')
                .unsigned()
                .references('id')
                .inTable('users');
            table.integer('scope_id')
                .unsigned()
                .references('id')
                .inTable('scopes')
        })
        /**
         * Notifications
         */
        .createTable('notifications', function (table) {
            table.increments();
            table.string('notification');
        })
        /**
         *
         */
        .createTable('user_notifications', function (table) {
            table.increments();
            table.integer('user_id')
                .unsigned()
                .references('id')
                .inTable('users');
            table.integer('notification_id')
                .unsigned()
                .references('id')
                .inTable('notifications');
        })
};

exports.down = function (knex, Promise) {
    return knex.schema
        .dropTable('user_notifications')
        .dropTable('notifications')
        .dropTable('user_scopes')
        .dropTable('scopes')
        .dropTable('user_socials')
        .dropTable('socials')
        .dropTable('users');
};
