exports.up = function (knex, Promise) {
    return knex.schema.createTable('events', function (table) {
        table.increments();
        table.integer('user_scope_id')
            .unsigned()
            .references('id')
            .inTable('user_scopes')
            .onDelete('CASCADE');  // optional
        table.string('image').default(null);
        table.string('title').default(null);
        table.string('info').default(null);
        table.string('social_vk').default(null);
        table.string('social_fb').default(null);
        table.string('social_twitter').default(null);
        table.timestamps();
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTable('events');
};
