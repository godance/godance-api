exports.up = function (knex, Promise) {

    return knex.schema.createTable('image_albums', function (table) {
            table.increments();
            table.integer('event_id')
                .unsigned()
                .references('id')
                .inTable('events')
                .onDelete('CASCADE');
            table.string('title');
            table.timestamps();
        })
        .createTable('images', function (table) {
            table.increments();
            table.integer('album_id')
                .unsigned()
                .references('id')
                .inTable('image_albums')
                .onDelete('CASCADE');
            table.string('cloudinary_id');
            table.timestamps();
        })

};

exports.down = function (knex, Promise) {
    return knex.schema
        .dropTable('images')
        .dropTable('image_albums');
};
