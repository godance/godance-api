const scopes = require('../../config/constants').scopes;

exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('scopes').del()
        .then(function () {
            return Promise.all(
                Object.keys(scopes).map(function (scope) {
                    return knex('scopes').insert({ scope: scopes[ scope ] })
                })
            );
        })
        .then(function () {
            return knex('users').insert({ email: 't@t.t', password: '1234' })
        });


    // return knex('users').insert({ email: 't@t.t', password: '1234' })

};
