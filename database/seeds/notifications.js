const notifications = require('../../config/constants').notifications;

exports.seed = function (knex, Promise) {
    return knex('socials').del()
        .then(function () {
            return Promise.all(
                Object.keys(notifications).map(function (notification) {
                    return knex('notifications').insert({ notification: notifications[ notification ] })
                })
            );
        });
};
