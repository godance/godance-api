const socials = require('../../config/constants').socials;

exports.seed = function (knex, Promise) {
    return knex('socials').del()
        .then(function () {
            return Promise.all(
                Object.keys(socials).map(function (social) {
                    return knex('socials').insert({ name: socials[ social ] })
                })
            );
        });
};
