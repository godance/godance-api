import test from 'tape';
import { _deDash, _filter, _mapSortItems } from '../../../src/http/query-params/sort';
/**
 *
 */
test('_deDash success', function (t) {
    t.plan(1);
    const deDashed = 'test';
    const dashed   = '-' + deDashed;
    t.equal(_deDash(dashed), deDashed, 'one equal one');
});
/**
 *
 */
test('_filter success', function (t) {
    t.plan(1);
    const sortable = [ 'id', 'image' ];
    const sort     = [ '-id', 'name', 'age' ];
    t.deepEqual(_filter(sort, sortable), [ '-id' ]);
});
/**
 *
 */
test('_mapSortItems success', function (t) {
    t.plan(1);
    const items       = 'id';
    const mappedItems =
              {
                  column: 'id',
                  order : 'ASC'
              };
    t.deepEqual(_mapSortItems(items), mappedItems);
});